from django.shortcuts import render,HttpResponse

# Create your views here.
def Home(request):
    return render(request,'index.html')

def Prediction(request):
    return render (request,'prediction.html')

def Price(request):
    return render (request, 'price.html')